import * as React from 'react';

interface Props {

}

interface State {
    history: Array<Step>;
    xIsNext: boolean;
    winner?: string;
    stepNo: number;
}

interface Step {
    squares: Array<string>
}

export default class Game extends React.Component<Props, State> {

    state: State = {
        history: [{
            squares: Array(9).fill(null)
        }],
        xIsNext: true,
        winner: undefined,
        stepNo: 0,
    };

    render() {
        const history:Array<Step> = this.state.history;
        const current:Step = history[this.state.stepNo];

        const moves = history.map((step, move) => {
            const descr = move ? 'Go to move #' + move : 'Go to game start';

            return (
                <li key={move}>
                    <button onClick={() => this.jumpTo(move)}>{descr}</button>
                </li>
            )
        });

        return (
            <div className="game">
                <div className="game-board">

                </div>
                <div className="game-info">
                    <div><h1>{this.getStatus()}</h1></div>
                    <div>
                        <ol>{moves}</ol>
                    </div>
                </div>
            </div>
        );
    }

    handleClick(i:number):void {
        const history = this.state.history.slice(0, this.state.stepNo + 1);
        const current = history[history.length - 1];
        const squares = current.squares.slice();

        if (!squares[i] && !this.state.winner) {
            squares[i] = this.getPlayer()
            const winner:string = this.calculateWinner(squares)

            this.setState({
                history: history.concat(
                    [{squares: squares}]
                ),
                xIsNext: !this.state.xIsNext,
                stepNo: history.length,
                winner: winner,
            })
        }
    }

    jumpTo(step: number):void {
        this.setState({
            history: [],
            stepNo: step,
            xIsNext: (step % 2) === 0,
            winner: undefined
        });
    }

    getStatus():string {
        if (this.state.winner) {
            return 'Winner: ' + this.state.winner;
        } else {
            if (this.state.stepNo === 9) {
                return 'DRAW'
            } else {
                return 'Next player: ' + this.getPlayer();
            }
        }
    }

    getPlayer():string {
        return this.state.xIsNext ? 'X' : 'O'
    }

    calculateWinner(squares: Array<string>):string {
        const lines = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6],
        ];

        for (let i = 0; i < lines.length; i++) {
            const [a, b, c] = lines[i];
            const aVal = squares[a];
            const bVal = squares[b];
            const cVal = squares[c];
            if (aVal && aVal === bVal && bVal === cVal) {
                return aVal;
            }
        }
        return "";
    }

}
